require("jsonlite")
require("RCurl")
require("ggplot2")
# Change the USER and PASS below to be your UTEid
#data frame for 5th graph
#df <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from titanic where sex is not null"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_mas8296', PASS='orcl_mas8296', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE), ))
#summary(df3)
#head(df3)

require(extrafont)
#custom plot
ggplot() + 
  coord_polar() + 
  scale_x_continuous() +
  scale_y_continuous(limits=c(-1,1)) +  #change y scale so deaths arent clustered in center
  labs(title='Titanic') +
  labs(x="FARE", y=paste("SURVIVED")) +
  
  layer(data=df, 
        mapping=aes(x=as.numeric(as.character(FARE)), y=SURVIVED, color=as.character(SEX)),  
        geom="point",
        stat="identity", 
        position=position_jitter(width=1.0, height=0)
  )